---
title: Nost (NoSaasTrust)
sub_title: a backup tool for owning them
author: Syslinbit
---

<!-- Durée 25 minutes -->
<!-- max 15 minutes présentation -->
<!-- min 10 minutes questions -->
<!-- Idée générale: rentrer dans le tas -->


<!-- cmd:jump_to_middle -->
Ça c'est nous
=============

Syslinbit: SCOP CAE en systèmes embarqués depuis 2022

=> Aider les développeurs à créer une activité impliquée dans les logiciels libres


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
Ça c'est pour vous connaître
============================

<!-- 1. qui parmi vous produit un Saas ? -->
<!-- 1. qui parmi 1/ mettent les backups à disposition des clients ? -->
<!-- 1. qui parmi 1/ accordent une confiance totale à leur gestion des backups ? -->
1. Question 1
1. Question 2
1. Question 3

<!-- cmd:end_slide -->
Le SaaS de la vie
=================


```
                                -.-..m ..
                          .  . -- -....... -+  -+..-.  .+. .   - .
                         +m  ...m-- .-...+m- - -...-+--+.-.m..m  .. -
                    .  -.+..m+-%*#%#-*+#+++.m-+ +--.%---. ++-%+-m.-..+ m  . .
                    --.%+-####-m +.-++-m#m%#--*++#m#---#*%##*##%+.*#-+ %...+.
                 ...+++..+m ..+.-. -+ . .-++ ..-.-% % +  +.--..%m- --m%%*.%+ -..
             ...-+% -%+# --- -  .  .   -.- . ...+.+   +.-. -- . .- ++-.*##*#m.-
            .   ---*#++m.-.   ..        .. . .  -+ .+ . .  .   -.. -+m.+-*++% .    +..
        + -mm--- +##++. .                                           . -.-. .+..     .- .- -
     + --.%+##*#-*.-. .                                                    .    -.m.-m----+m .
   .+++*m*m##-+--.-..                                                         .   --####%+*m
   --%m###m %+.. ..          +------------+           +------------+            -- --m#+#*#%.
 . ++.##-.+-.m...            | backups    |           |            |              -...-.--#m%+.
 ..+%#*mm. .-                |   via      |           |  mails ?   |                   ..+m#+.
   +m#-m+.                   |     ssh    |           |            |                      +###..
 -m*m.+                      +------------+           +------------+                    .-m#*#+
m.#.--                                                                                    .-%*-.+
m-#+-                                    -----------                                      -.-#*...
%##*-                                 --/           \--                                    --m# ..
+m.+..          +-------------+      /                 \         +---------------+         +-+#% .
%%+m .          | cloud       |     (         US        )        |  backup       |         -..%m-.+
++#m+.          |   synchro   |      \                 /         |    via        |          -+%#m
+mm.--          |      webdav |       --\           /--          |       API     |          ..+#- -
mm#+-+.         +-------------+          -----------             +---------------+          ..-#.
% *#-%.                                                                                     . +#m.
m++-.m.                                                                                       *%%.
.+.*m.% .                                   +--------------+                                 m##--
 -m%-m  .           +------------+          |  data        |                                .+##+-
 --m##m. . ..       |  configs   |          |    via       |                               -.-*+  .
 .m.m* - -- .       |     via    |          |      ssh     |                              -+mm+ ..
  .---%#%.          |       API  |          +--------------+                             -.. -m..
    -.m ++%...      +------------+                                                      --..*---.
    -.-*.-#m..   .+  .                                                      .    .  ..  .-+##+m-.
       ##.+-%%.- .. .++ -  ..  -    ..                .  .-..- .     .     +.--.- ...   . -*%m-.
     -...#*##%--- + ..-+.. +.+.m.+.-  .  . .      -.+--m .-m.- .. --..%.- . -.-+..... *-  #-+.
      ..---m+*%#++-**.+m%--+- +++...--.+  .      .. -+%m% ++.--#-+m--m.--+m.#mm-mm..%-. ..-.-
      . .+.-.%mm-.*- +%%-m#+##+m#%#***-.+- .----..m+-#.*%###m## mm m###*#%*-----   ...  ... -
          -.--. .. -- ..m---+.%m*--++#*#+mm#- #-%#m#*---+%-m.+%#.###m*#m+----. .       - .
             - ......  . --- .-.- +.+--+.++*+-##.++% -.- -  +.+.++mm% -.-.  .    .   .
                       .  . . .   -.  ..- +-.-- --..+ -. --.- m . -.- -m
                                      .   +    .  -..-           +  ...
```

<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
un désaastre est vite arrivé
============================

Que peut-il arriver ?

<!-- cmd:pause -->

| Tout                      | peut                    | arriver          |
|---------------------------|-------------------------|------------------|
| rupture du contrat        | faillite du fournisseur | erreur technique |
| destruction du datacenter |                         | bug logiciel     |
|                           | cyberattaque            | ransomware       |
|                           |                         |                  |


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
Saas passe près de chez vous
=============================

| Tout                   | peut | arriver         |
|------------------------|------|-----------------|
|                        |      |                 |
| incendie du datacenter |      | faille ext4 sur |
| OVH en mars 2021       |      | le noyau 6.5    |
|                        |      |                 |


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
Les préconisaassions
====================

## 3 réplications
## 2 supports
## 1 copie hors site


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
Problème ?
==========

## Notre coopérative est en gestion partagée

- les permissions peuvent être distribuées...
  - qui s'en occupe ?
  - => tout le monde ?

## Ce n'est pas notre métier

## Où stocker ? (serveurs, locaux...)

<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
nost: des backups sensaas
=========================

## Récupération et archivage des backups

### Un fichier unique à partager pour relancer la récupération des backups

### Des plugins pour gérer les sources

- rsync (+ssh)
- webdav

### Un backend pour l'archivage

- chiffrement
- rotations


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
nost: des backups sensaas
=========================

## C'est simple

## C'est automatisable ou manuel

## On peut le lancer dans un utilisateur particulier

## Tous les composants sont configurables

## Des vaults pour sauvegarder les mots de passe


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
nost: des backups sensaas
=========================

## Commandes

### Sync

1. fetch
1. encrypt
1. archive
1. rotate

### List

1. Affiche la liste des backups avec un identifiant

### Retrieve

1. Récupère une backup à partir de l'identifiant

### Delete

1. Supprime une backup à partir de l'identifiant


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
Améliorations
=============

- L'interface utilisateur
- Les plugins (mails, etc.)
- Les tests
- Les vaults (gestion des credentials)
- Les slides


<!-- cmd:end_slide -->
<!-- cmd:jump_to_middle -->
Questions
=========


