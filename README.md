# nost Slides

## Requirements

- presenterm
- presenterm-export (optional)

Run this with presenterm:

```bash
presenterm -c config.yaml nost.md
```

Or export in PDF with (not recommended):

```bash
presenterm -c config.yaml -e nost.md
```
